/* gmpc-favorites (GMPC plugin)
 * Copyright (C) 2007-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef __FAVORITES_H__
#define __FAVORITES_H__

#define FAV_CONFIG "favorites-plugin"

void favorites_init(void);
void favorites_destroy(void);
/* connection changed callback */
void favorites_conn_changed(MpdObj *mi, int connect, void *userdata);
/* browser */
void favorites_browser_add(GtkWidget *cat_tree);
void favorites_browser_selected(GtkWidget *container);
void favorites_browser_unselected(GtkWidget *container);
void favorites_browser_changed(GtkWidget *tree, GtkTreeIter *iter);
void favorites_browser_fill_list(void);
int favorites_browser_cat_menu(GtkWidget *menu, int type, GtkWidget *tree, GdkEventButton *event);
void favorites_browser_row_activated(GtkTreeView *tree, GtkTreePath *path, GtkTreeViewColumn *col, gpointer data);
/* mw intergration */
int favorites_go_menu(GtkWidget *menu);
int favorites_key_press(GtkWidget *mw, GdkEventKey *event, int type);
/**
 */
void favorites_add_current_song(void);
void favorites_save(void);
void favorites_create_playlist(void);
void favorites_clear(void);
#endif
