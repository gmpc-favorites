/* gmpc-favorites (GMPC plugin)
 * Copyright (C) 2007-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <gdk/gdkkeysyms.h>


#include <gmpc/plugin.h>
#include <gmpc/playlist3-messages.h>
#include <gmpc/gmpc-mpddata-model.h>
#include <libmpd/libmpd.h>
#include <config.h>
#include "favorites.h"


static GtkTreeRowReference *favorites_browser_ref = NULL;
static GtkWidget *favorites_browser = NULL;
gmpcPlugin plugin;

GmpcMpdDataModel *favorites_list_store = NULL;

/**
 * Enable/Disable plugin
 */
static int favorites_get_enabled(void)
{
	return cfg_get_single_value_as_int_with_default(config, FAV_CONFIG, "enable", TRUE);
}
static void favorites_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, FAV_CONFIG, "enable", enabled);
};

/**
 * Destroy Plugin
 */
void favorites_destroy(void)
{
  /* Cleanup open config */
  if(favorites_browser)
  {
    gtk_widget_destroy(favorites_browser);
    favorites_browser = NULL;
  }
}


static void favorites_status_changed(MpdObj *mi, ChangedStatusType what, void *data)
{
    if(what&MPD_CST_STORED_PLAYLIST)
    {
        favorites_browser_fill_list();
    }
}
/** 
 * Browser
 */
void favorites_browser_add(GtkWidget *cat_tree)
{
  GtkListStore *pl3_tree = playlist3_get_category_tree_store();
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
  /* Check if enabled */
  if(!cfg_get_single_value_as_int_with_default(config, FAV_CONFIG, "enable", TRUE)) return;

	gtk_list_store_append(pl3_tree, &iter);
	gtk_list_store_set(pl3_tree, &iter, 
			PL3_CAT_TYPE, plugin.id,
			PL3_CAT_TITLE, "Favorites Browser",
			PL3_CAT_INT_ID, "",
			PL3_CAT_ICON_ID, "emblem-favorite",
			PL3_CAT_PROC, TRUE,
			PL3_CAT_ICON_SIZE,GTK_ICON_SIZE_DND,-1);
	/**
	 * Clean up old row reference if it exists
	 */
	if (favorites_browser_ref)
	{
		gtk_tree_row_reference_free(favorites_browser_ref);
		favorites_browser_ref = NULL;
	}
	/**
	 * create row reference
	 */
	path = gtk_tree_model_get_path(GTK_TREE_MODEL(playlist3_get_category_tree_store()), &iter);
	if (path)
	{
		favorites_browser_ref = gtk_tree_row_reference_new(GTK_TREE_MODEL(playlist3_get_category_tree_store()), path);
		gtk_tree_path_free(path);
	}
}

void play_path(const gchar *path);
void favorites_browser_row_activated(GtkTreeView *tree, GtkTreePath *path, GtkTreeViewColumn *col, gpointer data)
{
  GtkTreeModel *model = gtk_tree_view_get_model(tree);
  GtkTreeIter iter;
  if(gtk_tree_model_get_iter(model, &iter, path))
  {
    gchar *spath = NULL;
    gtk_tree_model_get(model, &iter, MPDDATA_MODEL_COL_PATH, &spath, -1);
    if(spath)
    {
        play_path(spath);
        g_free(spath);
    }
  }
}

static void favorites_browser_init()
{
  GtkWidget *tree = NULL;
  GtkWidget *sw = NULL;
  GtkWidget *label;
  /* */
  favorites_browser = gtk_vbox_new(FALSE,0);

  /* Scroll window */
  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw), GTK_SHADOW_ETCHED_IN);
  gtk_box_pack_start(GTK_BOX(favorites_browser), sw, TRUE,TRUE,6);


  label = gtk_label_new("To favor the current playing song, press Ctrl-Enter");
  gtk_misc_set_alignment(GTK_MISC(label), 0,0.5);
  gtk_box_pack_start(GTK_BOX(favorites_browser), label, FALSE,TRUE,6);

  /* Model */
  favorites_list_store = gmpc_mpddata_model_new(); 
  /* treeview */
  tree = gmpc_mpddata_treeview_new("favorite-plugin", FALSE, GTK_TREE_MODEL(favorites_list_store)); 
  gtk_container_add(GTK_CONTAINER(sw), tree);

  g_signal_connect(G_OBJECT(tree), "row-activated", G_CALLBACK(favorites_browser_row_activated), NULL);
  g_object_ref(favorites_browser);

  favorites_browser_fill_list();
}

void favorites_browser_selected(GtkWidget *container)
{
  if(!favorites_browser) {
    favorites_browser_init();
  }
  gtk_container_add(GTK_CONTAINER(container), favorites_browser);
  gtk_widget_show_all(favorites_browser);
}

void favorites_browser_unselected(GtkWidget *container)
{
  gtk_container_remove(GTK_CONTAINER(container), favorites_browser);
}

void favorites_browser_fill_list(void)
{
  if(favorites_list_store)
  {
    MpdData *data = mpd_database_get_playlist_content(connection, "Favorites");
    gmpc_mpddata_model_set_mpd_data(favorites_list_store, data);
  }
}

int favorites_browser_cat_menu(GtkWidget *menu, int type, GtkWidget *tree, GdkEventButton *event)
{
  if(type == plugin.id)
  {
    GtkWidget *item;
    /** */
    item = gtk_image_menu_item_new_with_label("Load favorites playlist");
    gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), gtk_image_new_from_stock(GTK_STOCK_NEW, GTK_ICON_SIZE_MENU));
    g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(favorites_create_playlist), NULL);
    /* add to menu */
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    /** */
    item = gtk_image_menu_item_new_with_label("Clear favorites");
    gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), gtk_image_new_from_stock(GTK_STOCK_CLEAR, GTK_ICON_SIZE_MENU));
    g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(favorites_clear), NULL);
    /* add to menu */
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);


    return 2;
  }
  return 0;
}

/**
 * MW Intergration
 */
int favorites_go_menu(GtkWidget *menu)
{
  GtkWidget *item;
  /** */
  item = gtk_image_menu_item_new_with_label("Add Current Song to favorites");
  gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), gtk_image_new_from_icon_name("emblem-favorite", GTK_ICON_SIZE_MENU));
  gtk_widget_add_accelerator(GTK_WIDGET(item), "activate", gtk_menu_get_accel_group(GTK_MENU(menu)), GDK_Return, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(favorites_add_current_song), NULL);

  /* add to menu */
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  return 1;
}

void favorites_add_current_song(void)
{
  mpd_Song *song = mpd_playlist_get_current_song(connection);
  if(song->file) 
  {
      mpd_database_playlist_list_add(connection, "Favorites", song->file); 
  }
  else {
    printf("no song to add\n");
  }
}

void favorites_create_playlist(void)
{
    mpd_playlist_queue_load(connection, "Favorites");
    mpd_playlist_queue_commit(connection);
}

void favorites_clear(void)
{
    mpd_database_playlist_clear(connection, "Favorites");
}
static void gmtv_add_to_favorites(GtkMenuItem *item, GmpcMpdDataTreeview *tree)
{
    GList *selected = NULL;
    GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
    GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree));
    selected = gtk_tree_selection_get_selected_rows(selection, &model);
    if(selected)
    {
        GtkTreeIter titer;
        GList *iter = g_list_first(selected);
        for(;iter; iter = g_list_next(iter))
        {
            if(gtk_tree_model_get_iter(model, &titer, iter->data))
            {

                mpd_Song *song = NULL;
                gtk_tree_model_get(model, &titer, MPDDATA_MODEL_COL_MPDSONG, &song, -1);
                if(song  && song->file)
                {
                    mpd_database_playlist_list_add(connection, "Favorites", song->file); 
                }
            }
        }
        g_list_foreach (selected, (GFunc)gtk_tree_path_free, NULL);
        g_list_free (selected);
    }
}


int song_list_option_menu (GmpcMpdDataTreeview *tree, GtkMenu *menu)
{
    int retv = 0;
    GtkWidget *item;
    if(plugin.get_enabled())
    {
        item = gtk_image_menu_item_new_with_label("Add to favorites");
        gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), 
                gtk_image_new_from_icon_name("emblem-favorite", GTK_ICON_SIZE_MENU));
        gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
        g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(gmtv_add_to_favorites), tree);
        retv++;
    }
    return retv;
}

/**
 * Browser extention 
 */
gmpcPlBrowserPlugin favorites_gbp = {
  .add = favorites_browser_add,   /* Add */
  .selected = favorites_browser_selected,   /* Selected */
  .unselected = favorites_browser_unselected,   /* Unselected */
  .cat_right_mouse_menu = favorites_browser_cat_menu,   /* */
  .add_go_menu = favorites_go_menu, 
  .song_list_option_menu = song_list_option_menu
};

/**
 * Main plugin 
 */
int plugin_api_version = PLUGIN_API_VERSION;

gmpcPlugin plugin = {
  .name                         = "Favorites Plugin",
  .version                      = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
  .plugin_type                  = GMPC_PLUGIN_PL_BROWSER, 
  .destroy                      = favorites_destroy,
  .browser                      = &favorites_gbp,        
  .mpd_status_changed           = favorites_status_changed,
  .get_enabled                  = favorites_get_enabled,
  .set_enabled                  = favorites_set_enabled 
};
